﻿using System.ComponentModel.DataAnnotations;

namespace BackendProyecto.Models
{
    public class TipoUsuario
    {
        [Key]
        public int IdTipoUsuario { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
    }
}
