﻿using BackendProyecto.Data;
using BackendProyecto.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackendProyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly AppDbContext _db;
        public UsuarioController(AppDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public async Task<IActionResult> GetUsuarios()
        {
            var lista = await _db.Usuarios.OrderBy(p => p.Nombre).Include(p => p.TipoUsuario).ToListAsync();
            return Ok(lista);
        }
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetUsuario(int id)
        {
            var obj = await _db.Usuarios.Include(p => p.TipoUsuario).FirstOrDefaultAsync(p => p.TipoUsuarioId == id);
            if (obj == null)
            {
                return BadRequest();
            }
            return Ok(obj);
        }
        [HttpPost]
        public async Task<IActionResult> CrearUsuario([FromBody] Usuario usuario)
        {
            if (usuario == null)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _db.AddAsync(usuario);
            await _db.SaveChangesAsync();
            return Ok();
        }
    }
}
