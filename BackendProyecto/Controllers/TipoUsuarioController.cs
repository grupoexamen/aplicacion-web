﻿using BackendProyecto.Data;
using BackendProyecto.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BackendProyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoUsuarioController : ControllerBase
    {
        private readonly AppDbContext _db;
        public TipoUsuarioController(AppDbContext db)
        {
            _db = db;
        }
        [HttpGet]
        public async Task<IActionResult> GetTipoUsuarios()
        {
            var lista = await _db.TipoUsuarios.OrderBy(c => c.Nombre).ToListAsync();
            return Ok(lista);
        }
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetTipoUsuario(int id)
        {
            var obj = await _db.TipoUsuarios.FirstOrDefaultAsync(c => c.IdTipoUsuario == id);
            if (obj == null)
            {
                return NotFound();
            }
            return Ok(obj);
        }
        [HttpPost]
        public async Task<IActionResult> CrearTipoUsuario([FromBody] TipoUsuario tipoUsuario)
        {
            if (tipoUsuario == null)
            {
                return BadRequest(ModelState);
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _db.AddAsync(tipoUsuario);
            await _db.SaveChangesAsync();
            return Ok();
        }
        [HttpPut("{id}")]
        public async Task<ActionResult> PutTipoUsuario(int id, TipoUsuario tipoUsuario)
        {
            if (id == tipoUsuario.IdTipoUsuario)
            {
                _db.Entry(tipoUsuario).State = EntityState.Modified;
                await _db.SaveChangesAsync();
                return NoContent();
            }
            return BadRequest();
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteTipoUsuario(int id)
        {
            var xd = await _db.TipoUsuarios.FindAsync(id);
            if (xd == null)
            {
                return NotFound();
            }
            _db.TipoUsuarios.Remove(xd);
            await _db.SaveChangesAsync();
            return NoContent();
        }
    }
}
