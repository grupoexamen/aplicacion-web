﻿using BackendProyecto.Models;
using Microsoft.EntityFrameworkCore;

namespace BackendProyecto.Data
{
    public class AppDbContext:DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> option) : base(option)
        {

        }
        public DbSet<TipoUsuario> TipoUsuarios { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
    }
}
